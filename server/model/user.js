const Sequelize = require("sequelize");
module.exports = sequelize => {
    const User = sequelize.define('user', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        name: Sequelize.STRING,
        city: Sequelize.STRING,
        address: Sequelize.STRING,
        createdAt: {
            type:Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
        updatedAt: {
            type:Sequelize.DATE,
            defaultValue: Sequelize.NOW,
        },
      })
    return User;
};