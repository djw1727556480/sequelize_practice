module.exports = (app,sequelize) => {
    const router = require("koa-router")();
    app.use(router.routes()).use(router.allowedMethods());
    router.get(`/:item`, async ctx => {
        const Model = require(`../model/${ctx.params.item}`)(sequelize);
        const data = await Model.findAll({ limit: 10, offset: 0 }); // offset代表忽略前面多少条
        ctx.body = {
            success: true,
            code: 200,
            data
        };
    });
    router.get("/:item/:id", async ctx => {
        const Model = require(`../model/${ctx.params.item}`)(sequelize);
        const data = await Model.findOne({ where: { id: ctx.params.id }});
        ctx.body = {
            success: true,
            code: 200,
            data
        }
    });
    router.post("/:item", async ctx => {
        const Model = require(`../model/${ctx.params.item}`)(sequelize);
        await Model.create(ctx.request.body);
        ctx.body = { 
            success: true,
            code: 200,
        }
    });
    router.put("/:item/:id", async ctx => {
        const Model = require(`../model/${ctx.params.item}`)(sequelize);
        await Model.update(ctx.request.body,{ where: {id: ctx.params.id }});
        ctx.body = {
            success: true,
            code: 200,
        }
    })
    router.delete("/:item/:id", async ctx => {
        const Model = require(`../model/${ctx.params.item}`)(sequelize);
        await Model.destroy({ where: {id: ctx.params.id }});
        ctx.body = {
            success: true,
            code: 200,
        }
    })
}