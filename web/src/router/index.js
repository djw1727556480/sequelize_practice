import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import User from '../views/User.vue'
import Add from '../views/add.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [
      {
        path: '/user',
        name: '/user',
        component: User,
      },
      {
        path: '/add',
        name: '/add',
        component: Add,
      }
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
